<?php
/**
 * @file
 * in_delta_blocks_zen.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function in_delta_blocks_zen_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
}
